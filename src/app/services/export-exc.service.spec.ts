import { TestBed, inject } from '@angular/core/testing';

import { ExportExcService } from './export-exc.service';

describe('ExportExcService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExportExcService]
    });
  });

  it('should be created', inject([ExportExcService], (service: ExportExcService) => {
    expect(service).toBeTruthy();
  }));
});
