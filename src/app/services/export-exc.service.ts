import { Injectable } from '@angular/core';
import * as Excel from "exceljs/dist/exceljs.min.js";
import { Workbook } from 'exceljs/dist/exceljs.min.js';
import * as fs from 'file-saver';
import * as ExcelProper from "exceljs";
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Injectable()
export class ExportExcService {

  constructor() { }
  public exportAsExcelFile(json: any, excelFileName: string,title:string,type:number, headersArray: any[]): void {
    //Excel Title, Header, Data
    let dateFormat = require('dateformat');
    const header = headersArray;
    const data = json;
    //Create workbook and worksheet
    let workbook = new Workbook();
    let worksheet;
    //Free Meal
    if(type==0)
    {
    worksheet = workbook.addWorksheet('Free Meal');
    //Add Row and formatting
    let titleRow = worksheet.addRow([title]);
    titleRow.font ={ name: 'Arial Black',family:2,size: 12, bold: true };
    worksheet.getCell('A1').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.addRow(['Date : ' +dateFormat(Date.now(), "mmmm dd, yyyy")]);
    worksheet.getCell('A2').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('A2').font = {bold: true }
    worksheet.mergeCells('A1:F1');
    worksheet.mergeCells('A2:F2');

    worksheet.addRow([]);

    let headerRow = worksheet.addRow(header);
      
    headerRow.eachCell((cell, number) => {
      cell.font = {bold: true }
      if(cell.value=="Date")
        cell.border = { top: { style: 'medium' }, left: { style: 'medium' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      else if(cell.value=="Created at")
        cell.border = { top: { style: 'medium' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'medium' } }         
      else if(cell.value=="")
      {}
      else
      cell.border = { top: { style: 'medium' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      })
    let loop=0;
    // Add Data and Conditional Formatting
    data.forEach(d => {
      let scdate;
      loop++;
      if(d.sched_date_start==d.sched_date_end)
        scdate =dateFormat(d.sched_date_start, "mmmm dd, yyyy")
      else
        scdate =dateFormat(d.sched_date_start, "mmmm dd, yyyy")+" - "+dateFormat(d.sched_date_end, "mmmm dd, yyyy");
      let sctime=dateFormat("2019/8/1 "+d.sched_start,"h:MM:ss TT")+" - "+dateFormat("2019/8/1 "+d.sched_end,"h:MM:ss TT");
      
      let row = worksheet.addRow(["",scdate,parseFloat(d.sched_amount),sctime,d.sched_note,dateFormat(d.sched_created_at,"mmmm dd, yyyy h:MM:ss TT")]);
      let Cdate=row.getCell(2);
      let Cprice=row.getCell(3);
      let Ctime=row.getCell(4);
      let Cnote=row.getCell(5);
      let Ccreated=row.getCell(6);
      Cprice.numFmt='0.00';
      if(loop<data.length)
      {
        Cdate.border = { top: { style: 'thin' }, left: { style: 'medium' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        Cprice.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        Ctime.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        Cnote.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        Ccreated.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'medium' } }
      }
      else
      {         
        Cdate.border = { top: { style: 'thin' }, left: { style: 'medium' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
        Cprice.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
        Ctime.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
        Cnote.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
        Ccreated.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'medium' }, right: { style: 'medium' } }
      }
    
    }
    );
    worksheet.getColumn(2).width = 35;
    worksheet.getColumn(3).width = 15;
    worksheet.getColumn(4).width = 28;
    worksheet.getColumn(5).width = 30;
    worksheet.getColumn(6).width = 30;
    worksheet.addRow([]);
    }
    //Employee
    else if(type==1)
    {
    worksheet = workbook.addWorksheet('Employee');
    //Add Row and formatting
    let titleRow = worksheet.addRow([title]);
    titleRow.font ={ name: 'Arial Black',family:2,size: 12, bold: true };
    worksheet.getCell('A1').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.addRow(['Date : ' +dateFormat(Date.now(), "mmmm dd, yyyy")]);
    worksheet.getCell('A2').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('A2').font = {bold: true }
    worksheet.mergeCells('A1:E1');
    worksheet.mergeCells('A2:E2');

    worksheet.addRow([]);

    let headerRow = worksheet.addRow(header);
      
      headerRow.eachCell((cell, number) => {
        cell.font = {bold: true }
        if(cell.value=="ID No.")
          cell.border = { top: { style: 'medium' }, left: { style: 'medium' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        else if(cell.value=="Department")
          cell.border = { top: { style: 'medium' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'medium' } }         
        else if(cell.value=="")
        {}
        else
        cell.border = { top: { style: 'medium' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        })
    let loop=0;
    data.forEach(d => {
      let row = worksheet.addRow(["",parseInt(d.emp_id),d.emp_lname+", "+ d.emp_fname+" "+ d.emp_mname,d.emp_department]);
      loop++;
      let Cid=row.getCell(2);
      let Cname=row.getCell(3);
      let Cdept=row.getCell(4);
      if(loop<data.length)
      {
        Cid.border = { top: { style: 'thin' }, left: { style: 'medium' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        Cname.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        Cdept.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'medium' } }
      }
      else
      {         
        Cid.border = { top: { style: 'thin' }, left: { style: 'medium' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
        Cname.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
        Cdept.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'medium' }, right: { style: 'medium' } }
      }
      worksheet.getColumn(2).width = 10;
      worksheet.getColumn(3).width = 50;
      worksheet.getColumn(4).width = 12;
    }
    );
    worksheet.addRow([]);
    }
    //Menu
    else if(type==2)
    {
      worksheet = workbook.addWorksheet('Menu');

      let titleRow = worksheet.addRow([title]);
      titleRow.font ={ name: 'Arial Black',family:2,size: 12, bold: true };
      worksheet.getCell('A1').alignment = { vertical: 'middle', horizontal: 'center' };
      worksheet.addRow(['Date : ' +dateFormat(Date.now(), "mmmm dd, yyyy")]);
      worksheet.getCell('A2').alignment = { vertical: 'middle', horizontal: 'center' };
      worksheet.getCell('A2').font = {bold: true }
      worksheet.mergeCells('A1:E1');
      worksheet.mergeCells('A2:E2');

      worksheet.addRow([]);

      let headerRow = worksheet.addRow(header);
      
      headerRow.eachCell((cell, number) => {
        cell.font = {bold: true }
        if(cell.value=="Menu Name")
          cell.border = { top: { style: 'medium' }, left: { style: 'medium' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        else if(cell.value=="Price")
          cell.border = { top: { style: 'medium' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'medium' } }         
        else if(cell.value=="")
        {}
        else
        cell.border = { top: { style: 'medium' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        })
      let loop=0;
      data.forEach(d => {
        loop++;
        let cat="";
        if(d.menu_category==0)
          cat="Meal";
        else if(d.menu_category==1)
          cat="Beverage";
        else if(d.menu_category==2)
          cat="Side Meal";
        else if(d.menu_category==3)
          cat="Snacks";

        let row = worksheet.addRow(["",d.menu_name,cat,parseFloat(d.menu_price)]);
        let CMname=row.getCell(2);
        let Ccategory=row.getCell(3);
        let cprice=row.getCell(4);
        cprice.numFmt='0.00';
        if(loop<data.length)
        {
          CMname.border = { top: { style: 'thin' }, left: { style: 'medium' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
          Ccategory.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
          cprice.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'medium' } }
        }
        else
        {         
          CMname.border = { top: { style: 'thin' }, left: { style: 'medium' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
          Ccategory.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
          cprice.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'medium' }, right: { style: 'medium' } }
        }  
      }
      );
      worksheet.getColumn(2).width = 20;
      worksheet.getColumn(3).width = 20;
      worksheet.getColumn(4).width = 15;
      worksheet.addRow([]);
    }
    //Accounts
    else
    {
      worksheet = workbook.addWorksheet('Accounts');
      //Add Row and formatting
      let titleRow = worksheet.addRow([title]);
      titleRow.font ={ name: 'Arial Black',family:2,size: 12, bold: true };
      worksheet.getCell('A1').alignment = { vertical: 'middle', horizontal: 'center' };
      worksheet.addRow(['Date : ' +dateFormat(Date.now(), "mmmm dd, yyyy")]);
      worksheet.getCell('A2').alignment = { vertical: 'middle', horizontal: 'center' };
      worksheet.getCell('A2').font = {bold: true }
      worksheet.mergeCells('A1:E1');
      worksheet.mergeCells('A2:E2');

      worksheet.addRow([]);

      let headerRow = worksheet.addRow(header);
      
      headerRow.eachCell((cell, number) => {
        cell.font = {bold: true }
        if(cell.value=="ID No.")
          cell.border = { top: { style: 'medium' }, left: { style: 'medium' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        else if(cell.value=="Role")
          cell.border = { top: { style: 'medium' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'medium' } }         
        else if(cell.value=="")
        {}
        else
        cell.border = { top: { style: 'medium' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        })
      let loop=0;
      data.forEach(d => {
        let role;
        loop++;
        if(d.usr_role==0)
          role="Admin";
        else if(d.usr_role==1)
          role="Cashier";
        else if(d.usr_role==2)
          role="Fruit Distributor";
        let row = worksheet.addRow(["",parseInt(d.emp_id),d.usr_name,d.usr_username,role]); 
        let Cid=row.getCell(2);
        let Cname=row.getCell(3);
        let Cusername=row.getCell(4);
        let Crole=row.getCell(5);
        if(loop<data.length)
        {
          Cid.border = { top: { style: 'thin' }, left: { style: 'medium' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
          Cname.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
          Cusername.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
          Crole.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'medium' } }
        }
        else
        {         
          Cid.border = { top: { style: 'thin' }, left: { style: 'medium' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
          Cname.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
          Cusername.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
          Crole.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'medium' }, right: { style: 'medium' } }
        }       
      }
      );
      worksheet.getColumn(2).width = 7;
      worksheet.getColumn(3).width = 50;
      worksheet.getColumn(4).width = 20;
      worksheet.getColumn(5).width = 20;
      worksheet.addRow([]);
    }

    //Generate Excel File with given name
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: EXCEL_TYPE });
      fs.saveAs(blob, excelFileName + EXCEL_EXTENSION);
    })
  }
  public exportAsExcelFileMEAL(json: any, excelFileName: string,dtra:string,vsearch:string,type:number, headersArray: any[]): void {
    //Excel Title, Header, Data
    let dateFormat = require('dateformat');
    const header = headersArray;
    const CanteenName="(Canteen Name)";
    const data = json;
    //Create workbook and worksheet
    let workbook = new Workbook();
    let titlesub;
    let worksheet;
    if(type==0)
    { 
      titlesub="Credit Meals"
      worksheet = workbook.addWorksheet('Credit Report');
    }
    else
    {
      titlesub="Free Meals"
      worksheet = workbook.addWorksheet('Free Meal Report');
    }
    //Add Row and formatting
    let titleRow = worksheet.addRow([CanteenName]);
    titleRow.font = { name: 'Arial Black',family:2,size: 12, bold: true }
    worksheet.getCell('A1').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.addRow(["Summary of Billing"]);
    worksheet.getCell('A2').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('A2').font = { bold: true };
    worksheet.addRow([titlesub]);
    worksheet.getCell('A3').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('A3').font = { bold: true };
    worksheet.addRow(["Cut-off Date : "+dtra]);
    worksheet.getCell('A4').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('A4').font = { bold: true };
    worksheet.addRow([vsearch]);
    worksheet.getCell('A5').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('A5').font = { bold: true };

    worksheet.mergeCells('A1:E1');
    worksheet.mergeCells('A2:E2');
    worksheet.mergeCells('A3:E3');
    worksheet.mergeCells('A4:E4');
    worksheet.mergeCells('A5:E5');
    //Blank Row 
    worksheet.addRow([]);
    //Add Header Row
    let headerRow = worksheet.addRow(header);
    
    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.font = {bold: true }
      if(cell.value=="ID No.")
        cell.border = { top: { style: 'medium' }, left: { style: 'medium' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      else if(cell.value=="Date")
        cell.border = { top: { style: 'medium' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'medium' } }
      else if(cell.value=="")
      {}
      else
      cell.border = { top: { style: 'medium' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      })

    // Add Data and Conditional Formatting
    let loop=0;
    data.forEach(d => {
      let row;
      if(type==0)
        row= worksheet.addRow(["",parseInt(d.emp_id),d.emp_name,parseFloat(d.cr_amount),d.cr_date]);
      
      else
        row= worksheet.addRow(["",parseInt(d.emp_id),d.emp_name,parseFloat(d.fmeal_amount),d.fmeal_date]);
      
      loop++;
      let n = row.getCell(3);
      n.alignment={ vertical: 'middle', horizontal: 'left' };
      let qty=row.getCell(4);
      qty.numFmt = '0.00';

      if(loop==data.length-1)
        worksheet.addRow([]);

      let color = 'FF99FF99';
      if (n.value=="TOTAL FREE MEAL CREDIT:"||n.value=="TOTAL MEAL CREDIT:") {
        let Nnum=row.getCell(2);
        Nnum.value="";
        qty.fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: { argb: color }
        };
        qty.font={bold: true};
        n.font={bold: true};
        n.alignment={ vertical: 'middle', horizontal: 'right' };
        qty.border= { top: { style: 'medium' }, left: { style: 'thin' }, bottom: { style: 'medium' }, right: { style: 'medium' } }
        n.border= { top: { style: 'medium' }, left: { style: 'medium' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
        let cl=row.getCell(5);
        cl.value="";
      }
      else
      {
        //Border
        let Cid=row.getCell(2);
        let Cname=row.getCell(3);
        let Camount=row.getCell(4);
        let CDate=row.getCell(5);
        if(loop==data.length-1)
        {
          Cid.border = { top: { style: 'thin' }, left: { style: 'medium' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
          Cname.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
          Camount.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
          CDate.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'medium' }, right: { style: 'medium' } }
        }
        else
        {         
          Cid.border = { top: { style: 'thin' }, left: { style: 'medium' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
          Cname.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
          Camount.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
          CDate.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'medium' } }
        }         
      }

    }
    );
    //Setting Width Size
    worksheet.getColumn(2).width = 10;
    worksheet.getColumn(3).width = 50;
    worksheet.getColumn(4).width = 15;
    worksheet.getColumn(5).width = 20;
    worksheet.addRow([]);

    //Generate Excel File with given name
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: EXCEL_TYPE });
      fs.saveAs(blob, excelFileName + EXCEL_EXTENSION);
    })
  }
  public exportAsExcelFileFRUIT(json: any[], excelFileName: string,dtra:string,vsearch:string, headersArray: any[]): void {
    //Excel Title, Header, Data
    let dateFormat = require('dateformat');
    const header = headersArray;
    const data = json;
    const CanteenName="(Canteen Name)";
    //Create workbook and worksheet
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet('Fruits Distributed');
    //Add Row and formatting
    let titleRow = worksheet.addRow([CanteenName]);
    titleRow.font = {name: 'Arial Black',family:2,size: 16, bold: true }
    worksheet.getCell('A1').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.addRow(["Fruit Summary"]);
    worksheet.getCell('A2').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.addRow(["Cut-off : "+dtra]);
    worksheet.getCell('A3').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.addRow([vsearch]);
    worksheet.getCell('A4').alignment = { vertical: 'middle', horizontal: 'center' };

    worksheet.mergeCells('A1:E1');
    worksheet.mergeCells('A2:E2');
    worksheet.mergeCells('A3:E3');
    worksheet.mergeCells('A4:E4');
    //Blank Row 
    worksheet.addRow([]);
    //Add Header Row
    let headerRow = worksheet.addRow(header);
    
    // Setting Header
    headerRow.eachCell((cell, number) => {
      cell.font = {bold: true }
      if(cell.value=="ID No.")
        cell.border = { top: { style: 'medium' }, left: { style: 'medium' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      else if(cell.value=="Date")
        cell.border = { top: { style: 'medium' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'medium' } }
      else if(cell.value=="")
      {}
      else
      cell.border = { top: { style: 'medium' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    })
    //Setting Data
    let loop=0;
    data.forEach(d => {
      let row = worksheet.addRow(["",parseInt(d.emp_id),d.emp_name,parseInt(d.fruits_qty),d.fruits_date]);
      loop++;

      let n = row.getCell(3);
      n.alignment={ vertical: 'middle', horizontal: 'left' };
      if(loop==data.length-1)
        worksheet.addRow([]);
      
      let color = 'FF99FF99';
      //Formating Total
      if (n.value=="TOTAL FRUIT DISTRIBUTED:") {
        let Nnum=row.getCell(2);
        Nnum.value="";
        n.alignment={ vertical: 'middle', horizontal: 'right' };
        let qty=row.getCell(4);
        qty.fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: { argb: color }
        }
        qty.font={bold: true};
        n.font={bold: true};
        n.alignment={ vertical: 'middle', horizontal: 'right' };
        qty.border= { top: { style: 'medium' }, left: { style: 'thin' }, bottom: { style: 'medium' }, right: { style: 'medium' } }
        n.border= { top: { style: 'medium' }, left: { style: 'medium' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
      }
      else
      {
         //Border
         let Cid=row.getCell(2);
         let Cname=row.getCell(3);
         let Camount=row.getCell(4);
         let Cdate=row.getCell(5)
         if(loop==data.length-1)
         {
           Cid.border = { top: { style: 'thin' }, left: { style: 'medium' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
           Cname.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
           Camount.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
           Cdate.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'medium' }, right: { style: 'medium' } }
         }
         else
         {         
           Cid.border = { top: { style: 'thin' }, left: { style: 'medium' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
           Cname.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
           Camount.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
           Cdate.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'medium' } }
         }         
      }
    }
    );
    //Setting Width Size
    worksheet.getColumn(2).width = 10;
    worksheet.getColumn(3).width = 50;
    worksheet.getColumn(4).width = 15;
    worksheet.getColumn(5).width = 20;
    worksheet.addRow([]);

    //Generate Excel File with given name
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: EXCEL_TYPE });
      fs.saveAs(blob, excelFileName + EXCEL_EXTENSION);
    })
  }
public exportAsExcelFileMEALEMP(json: any, excelFileName: string,dtra:string,empname:string,type:number, headersArray: any[]): void {
    //Excel Title, Header, Data
    let dateFormat = require('dateformat');
    const header = headersArray;
    const EmpName=empname;
    const data = json;
    //Create workbook and worksheet
    let workbook = new Workbook();
    let titlesub;
    let worksheet;
    if(type==0)
    { 
      titlesub="Credit Meals"
      worksheet = workbook.addWorksheet('Credit Report');
    }
    else
    {
      titlesub="Free Meals"
      worksheet = workbook.addWorksheet('Free Meal Report');
    }
    //Add Row and formatting
    let titleRow = worksheet.addRow([EmpName]);
    titleRow.font = { name: 'Arial Black',family:2,size: 12, bold: true }
    worksheet.getCell('A1').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.addRow(["Summary of Billing"]);
    worksheet.getCell('A2').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('A2').font = { bold: true };
    worksheet.addRow([titlesub]);
    worksheet.getCell('A3').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('A3').font = { bold: true };
    worksheet.addRow(["Cut-off Date : "+dtra]);
    worksheet.getCell('A4').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('A4').font = { bold: true };

    worksheet.mergeCells('A1:E1');
    worksheet.mergeCells('A2:E2');
    worksheet.mergeCells('A3:E3');
    worksheet.mergeCells('A4:E4');

    //Blank Row 
    worksheet.addRow([]);
    //Add Header Row
    let headerRow = worksheet.addRow(header);
    
    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.font = {bold: true }
      if(cell.value=="Amount")
        cell.border = { top: { style: 'medium' }, left: { style: 'medium' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      else if(cell.value=="Date")
        cell.border = { top: { style: 'medium' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'medium' } }
      else if(cell.value=="")
      {}
      else
      cell.border = { top: { style: 'medium' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
      })

    // Add Data and Conditional Formatting
    let loop=0;
    data.forEach(d => {
      let row;
      loop++;
      if(loop<data.length)
      {
        if(type==0)
          row= worksheet.addRow(["",parseFloat(d.cr_amount),d.cr_date]);
      
        else
          row= worksheet.addRow(["",parseFloat(d.fmeal_amount),d.fmeal_date]);
      }
      //Space before total
      if(loop==data.length-1)
        worksheet.addRow([]);
      
        //Display Total
      if(loop==data.length)
      {
        if(type==0)
          row= worksheet.addRow(["",parseFloat(d.cr_amount),": TOTAL"]);
        else
            row= worksheet.addRow(["",parseFloat(d.fmeal_amount),": TOTAL"]);
      }  
      let n = row.getCell(3);
      n.alignment={ vertical: 'middle', horizontal: 'center' };
      let qty=row.getCell(2);
      qty.numFmt = '0.00';

      let color = 'FF99FF99';
      if (n.value==": TOTAL") {
        qty.fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: { argb: color }
        };
        qty.font={bold: true};
        n.font={bold: true};
        n.alignment={ vertical: 'middle', horizontal: 'left' };
        qty.border= { top: { style: 'medium' }, left: { style: 'medium' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
        n.border= { top: { style: 'medium' }, left: { style: 'medium' }, bottom: { style: 'medium' }, right: { style: 'medium' } }
      }
      else
      {
        //Border
        let Camount=row.getCell(2);
        let Cdate=row.getCell(3);
        if(loop==data.length-1)
        {
          Camount.border = { top: { style: 'thin' }, left: { style: 'medium' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
          Cdate.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'medium' }, right: { style: 'medium' } }
        }
        else
        {         
          Camount.border = { top: { style: 'medium' }, left: { style: 'medium' }, bottom: { style: 'medium' }, right: { style: 'thin' } }
          Cdate.border = { top: { style: 'medium' }, left: { style: 'thin' }, bottom: { style: 'medium' }, right: { style: 'medium' } }
         
        }         
      }

    }
    );
    //Setting Width Size
    worksheet.getColumn(2).width = 10;
    worksheet.getColumn(3).width = 50;
    worksheet.getColumn(4).width = 15;
    worksheet.addRow([]);

    //Generate Excel File with given name
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: EXCEL_TYPE });
      fs.saveAs(blob, excelFileName + EXCEL_EXTENSION);
    })
  }
}
