import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DataService {
  userRole: number;
  //apiLink: string = 'http://techmatesph.com/michael/lindberg/apis/admin/'; //techmatesph server
  //apiLink: string = 'http://192.168.1.3/lindberg/apis/admin/';
  apiLink: string = 'http://localhost/techmates_rshs/apis/admin/'; //localhost

  constructor(public http:HttpClient) { }

   pullData(data, vAPI){
     return this.http.post(this.apiLink+vAPI+'.php', btoa(JSON.stringify(data)));
   }
}
