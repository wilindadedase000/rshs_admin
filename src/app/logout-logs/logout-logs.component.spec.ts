import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogoutLogsComponent } from './logout-logs.component';

describe('LogoutLogsComponent', () => {
  let component: LogoutLogsComponent;
  let fixture: ComponentFixture<LogoutLogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogoutLogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoutLogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
