import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { StudLogs,SingleStudent } from '../data-schema';
import { DataService } from '../services/data.service';
@Component({
  selector: 'app-login-logs',
  templateUrl: './login-logs.component.html',
  styleUrls: ['./login-logs.component.scss']
})
export class LoginLogsComponent implements OnInit {
  closeResult: string;
  tk: Object;
  stdlogs = new StudLogs();
  sngllogs=new SingleStudent();
  vData: Object=[];
  stData: Object=[];
  indx:Number;
  //Search
  vSearch:string="";
  vSearch_from:string="";
  vSearch_to:string="";
  constructor(private ds:DataService,private modalService: NgbModal) { }

  ngOnInit() {
  this.getloginlogs();
  }

  getloginlogs()
  {
    this.stdlogs.p_name="";
    this.stdlogs.p_date_from="";
    this.stdlogs.p_date_to="";
    this.stdlogs.p_isStudent=true;
    this.stdlogs.p_log=0;
    
    this.ds.pullData(this.stdlogs, "getlogsreport").subscribe((res)=>{
      this.vData=res[0].result;
    });
  }
  SearchnDate()
  {
    if(this.vSearch_to=="" && this.vSearch_from!="" )
    {
      this.vSearch_to=this.vSearch_from;
    }
    else
    {
      if(this.vSearch_to==undefined || this.vSearch_to<=this.vSearch_from)
      {  
        this.vSearch_to=this.vSearch_from;
      }
    }
    this.stdlogs.p_name=this.vSearch;
    this.stdlogs.p_date_from=this.vSearch_from;
    this.stdlogs.p_date_to=this.vSearch_to;
    this.stdlogs.p_isStudent=true;
    this.stdlogs.p_log=0;
    
    this.ds.pullData(this.stdlogs, "getlogsreport").subscribe((res)=>{
      this.vData=res[0].result;
    });
  }
  open(content,i) {
    this.indx=i;
    this.getstlogs(i);
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  getstlogs(i)
  {
    this.sngllogs.p_id=this.vData[i].p_studentid;
    this.sngllogs.p_log=0;
    this.ds.pullData(this.sngllogs, "getstudentlogs").subscribe((res)=>{
      this.stData=res[0].result;;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  print(): void {
    if(this.vData["length"]<=0)
    {
      alert("No Data to print");
      return;
    }
    let dateFormat = require('dateformat');
    let vHead = `
      <head>
        <style>
          body {
            margin: 10mm 10mm 10mm 10mm;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 10px
          }
          .print-headers {
            text-align: center; font-weight: bold; 
          }
  
          .column-headers {
            text-align: center; font-size: 10px;
          }
  
          table {
            border-collapse: collapse;
            width: 100%;
          }
          
          td, th {
            border: 1px solid #dddddd;
            text-align: left;
          }
  
          .columnCenter {
            text-align: center;
          }
          
          .recordContent {
            font-size: 10px;
          }
          .text-center{
            text-align:center;
          }
          .text-right{
            text-align:right;
          }
          .logo{
            position: absolute;
            left:225px; top:-5px;
            width: 200px;
            height:auto;
          }
          .signaturetitle {
            font-weight: bold;
            font-size: 150%;
            border-top: 1px solid black;
            width: 25%;
          }    
        </style>
      </head>`;
  
      let tblString=`<div>
      <table class="table " border="1">
        <thead>
            <th class="text-center" >Student Id</th>
            <th class="text-center">Student Name</th>
            <th class="text-center">Last Login</th>
        </thead>
        <tbody>`;
          for(var d=0; d<this.vData["length"];d++ )
          {
            tblString +=`<tr >
            <td class="text-center">`+this.vData[d]["p_studentid"]+`</td>
            <td class="text-left">`+this.vData[d]["p_name"]+`</td>
            <td class="text-center">`+dateFormat(this.vData[d]["l_timelog"],"mmm dd,yyyy h:MM TT")+`</td>
            </tr>`;
          }
        
      tblString+= `</tbody>
      </table>            
      `
      //<img src="http://localhost/Lindberg_Meal/dist/assets/img/logo_print_1liner.png" alt="" width="275" height="auto">
      let vBody = `
      <body onload="window.print();window.close()">
        <div class="logo">
          
        </div>
        <h3 class="print-headers"> Regional Science High School Region III <br>Logged In</h3>`;
        if(this.vSearch!="")
          vBody +="<br>Search: "+this.vSearch+"<br>";
        if(this.vSearch_from!="")
        {  if(this.vSearch_from==this.vSearch_to)
            vBody +="<br>Report Date: "+this.vSearch_from+"<br>";
          else
          vBody +="<br>Report Date<br> From: "+this.vSearch_from+" To: "+this.vSearch_to+"<br>";
        }
        vBody +="</h3><h3 class='text-right'>Date: "+dateFormat(Date.now(), "mmmm dd, yyyy")+" </h3>";
        vBody += tblString+`
        <br>
        <!--<p class="signaturetitle" style="float:left;">
        Signature:
      </p>
      <p class="signaturetitle" style="float:right;" >Signature:</p>-->
        </body>`;
    let printContents, popupWin;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`<html>`+vHead+vBody+`</html>`);
    popupWin.document.close();    
    popupWin.document.close();
  }
}
