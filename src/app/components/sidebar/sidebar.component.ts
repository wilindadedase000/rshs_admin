import { Component, OnInit } from '@angular/core';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    // { path: '/dashboard', title: 'Dashboard',  icon: 'design_app', class: '' },
    { path: '/menu', title: 'Dashboard',  icon: 'files_paper', class: '' },
    { path: '/loginLogs', title: 'Login Logs',  icon: 'arrows-1_minimal-right', class: '' },
    { path: '/logoutLogs', title: 'Logout Logs',  icon: 'arrows-1_minimal-left', class: '' },
    { path: '/announcement', title: 'Annoucements',  icon: 'ui-1_send', class: '' },
    // { path: '/freemeal', title: 'Free Meal',  icon: 'ui-1_bell-53', class: '' },
    // { path: '/employee', title: 'Employee',  icon: 'users_single-02', class: '' },
    // { path: '/accounts', title: 'Accounts',  icon: 'users_circle-08', class: '' },
    // { path: '/fruitreport', title: 'Fruit Reports',  icon: 'files_single-copy-04', class: '' },
    // { path: '/mealreport', title: 'Meal Reports',  icon: 'files_single-copy-04', class: '' }
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ( window.innerWidth > 991) {
          return false;
      }
      return true;
  };
}
