import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Announcement } from '../data-schema';
import { DataService } from '../services/data.service';
@Component({
  selector: 'app-announcement',
  templateUrl: './announcement.component.html',
  styleUrls: ['./announcement.component.scss']
})
export class AnnouncementComponent implements OnInit {
  closeResult: string;
  vData:Object;
  indx:number;
  constructor(private modalService: NgbModal,private ds: DataService) { }
  addAnn=new Announcement();
  ngOnInit() {
    this.ds.pullData(this.addAnn, "getann").subscribe((res)=>{
      this.vData=res[0].result;
    });
  }

insertAnn(e)
{
  e.preventDefault();

  var imgtag = document.getElementById("img-upload");
  
  this.addAnn.a_id=0;
  this.addAnn.a_title=e.target.elements[0].value;
  this.addAnn.a_announcement=e.target.elements[2].value;
  this.addAnn.a_postedby=e.target.elements[1].value;
  this.addAnn.a_image=imgtag["src"];
 
  if(this.valid())
  {  
    this.ds.pullData(this.addAnn, "insertann").subscribe((res)=>{
      this.vData=res[0].result;
    });
  }
  else
  {
    alert("Some TextBox are null");
  }

}
updateann(e,cm)
{
  e.preventDefault();

  var imgtag = document.getElementById("upd-img-upload");
  
  this.addAnn.a_id=this.vData[this.indx].a_recno;
  this.addAnn.a_title=e.target.elements[0].value;
  this.addAnn.a_announcement=e.target.elements[2].value;
  this.addAnn.a_postedby=e.target.elements[1].value;
  this.addAnn.a_image=imgtag["src"];
  
  if(this.valid())
  {  
  this.ds.pullData(this.addAnn, "updann").subscribe((res)=>{
    this.vData=res[0].result;
    if(res[0].status[0].remarks=="OK")
      cm();
  });
  }
  else
  {
    alert("Some TextBox are null");
  }
}

  open(content,i) {
    this.indx = i;
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  valid():boolean
  {
    if(this.addAnn.a_title==null|| this.addAnn.a_title=="")
      return false;
    else if(this.addAnn.a_announcement==null|| this.addAnn.a_announcement=="")
      return false;
    else if(this.addAnn.a_postedby==null|| this.addAnn.a_postedby=="")
      return false;
    return true;
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  ResizeImage(event) {
    var file = event.target.files[0];
  
    // Ensure it's an image
    if(file.type.match(/image.*/)) {
  
        // Load the image
        var reader = new FileReader();
        reader.onload = function (readerEvent) {
            var image = new Image();
            image.onload = function (imageEvent) {
  
                // Resize the image
                var MAX_WIDTH = 384;
                var MAX_HEIGHT = 680;
                var width = image.width;
                var height = image.height;
  
                var canvas = document.createElement('canvas'),
                    max_size = 544,
                    width = image.width,
                    height = image.height;
                    if (width > height) {
                      if (width > MAX_WIDTH) {
                          height *= MAX_WIDTH / width;
                          width = MAX_WIDTH;
                      }
                  } else {
                      if (height > MAX_HEIGHT) {
                          width *= MAX_HEIGHT / height;
                          height = MAX_HEIGHT;
                      }
                  }
                canvas.width = width;
                canvas.height = height;
                canvas.getContext('2d').drawImage(image, 0, 0, width, height);
                var dataUrl = canvas.toDataURL('image/jpeg');
                var resizedImage = dataURLToBlob(dataUrl);
  
                document.getElementById('img-upload')["src"] = dataUrl;
            }
            image.src = readerEvent.target["result"];
  
        }
        reader.readAsDataURL(file);
      }
      var dataURLToBlob = function(dataURL) {
        var BASE64_MARKER = ';base64,';
        if (dataURL.indexOf(BASE64_MARKER) == -1) {
            var parts = dataURL.split(',');
            var contentType = parts[0].split(':')[1];
            var raw = parts[1];
      
            return new Blob([raw], {type: contentType});
        }
      
        var parts = dataURL.split(BASE64_MARKER);
        var contentType = parts[0].split(':')[1];
         raw = window.atob(parts[1]);
        var rawLength = raw.length;
      
        var uInt8Array = new Uint8Array(rawLength);
      
        for (var i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }
      
        return new Blob([uInt8Array], {type: contentType});
      }
  }
  ResizeImageupd(event) {
    var file = event.target.files[0];
  
    // Ensure it's an image
    if(file.type.match(/image.*/)) {
  
        // Load the image
        var reader = new FileReader();
        reader.onload = function (readerEvent) {
            var image = new Image();
            image.onload = function (imageEvent) {
  
                // Resize the image
                var MAX_WIDTH = 384;
                var MAX_HEIGHT = 680;
                var width = image.width;
                var height = image.height;
  
                var canvas = document.createElement('canvas'),
                    max_size = 544,
                    width = image.width,
                    height = image.height;
                    if (width > height) {
                      if (width > MAX_WIDTH) {
                          height *= MAX_WIDTH / width;
                          width = MAX_WIDTH;
                      }
                  } else {
                      if (height > MAX_HEIGHT) {
                          width *= MAX_HEIGHT / height;
                          height = MAX_HEIGHT;
                      }
                  }
                canvas.width = width;
                canvas.height = height;
                canvas.getContext('2d').drawImage(image, 0, 0, width, height);
                var dataUrl = canvas.toDataURL('image/jpeg');
                var resizedImage = dataURLToBlob(dataUrl);
  
                document.getElementById('upd-img-upload')["src"] = dataUrl;
            }
            image.src = readerEvent.target["result"];
  
        }
        reader.readAsDataURL(file);
      }
      var dataURLToBlob = function(dataURL) {
        var BASE64_MARKER = ';base64,';
        if (dataURL.indexOf(BASE64_MARKER) == -1) {
            var parts = dataURL.split(',');
            var contentType = parts[0].split(':')[1];
            var raw = parts[1];
      
            return new Blob([raw], {type: contentType});
        }
      
        var parts = dataURL.split(BASE64_MARKER);
        var contentType = parts[0].split(':')[1];
         raw = window.atob(parts[1]);
        var rawLength = raw.length;
      
        var uInt8Array = new Uint8Array(rawLength);
      
        for (var i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }
      
        return new Blob([uInt8Array], {type: contentType});
      }
  }
}


