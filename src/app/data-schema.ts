export class PullDataOnly {
  token: string;
  vURL: string;
}
export class Credentials {
  username:string;
  password:string;
}
export class Announcement {
  token: string;
  vURL: string;
  a_id:number;
  a_title:string;
  a_announcement:string;
  a_postedby:string;
  a_image:string
  }
export class StudLogs {
  token: string;
  vURL: string;
    p_name:string;
    p_date_from:string;
    p_date_to:string;
    p_isStudent:boolean;
    p_log:number
    }
export class SingleStudent {
  token: string;
  vURL: string;
  p_id:string;
  p_log:number;
  }