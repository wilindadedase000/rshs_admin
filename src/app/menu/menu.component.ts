import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { PullDataOnly } from '../data-schema';
import { DataService } from '../services/data.service';
import { Observable,Subject } from 'rxjs';
import {debounceTime} from 'rxjs/operators';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  tk: Object;
  tc = new PullDataOnly();
  vData: Object=[
  {
    l_loggedin:0,
    l_loggedout:0
  }
  ];
  constructor(private ds:DataService,private modalService: NgbModal) { }
  closeResult: string;
  ngOnInit() {
    this.ds.pullData(this.tc, "getlogs").subscribe((res)=>{
      this.vData=res[0].result;
    });
  }

  open(content,indx){
    this.modalService.open(content, {'size': 'lg', 'centered': true }).result.then((result)=>{
      this.closeResult = 'Closed with: ${result}';
    }, (reason) =>{
      this.closeResult = 'Dismissed ${this.getDismissReason(reason)}';
    });
  }
  
  getDismissReason(reason: any) : string {
    if(reason === ModalDismissReasons.ESC) {
      return 'by Pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by Clicking on a Backdrop';
    } else {
      return 'with: ${reasonm}';
    }
  }

}
