import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { ChartsModule } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';

//components
// import { DashboardComponent } from '../../dashboard/dashboard.component';
// import { FreemealComponent } from '../../freemeal/freemeal.component';
// import { EmployeeComponent } from '../../employee/employee.component';
// import { AccountsComponent } from '../../accounts/accounts.component';
// import { FruitreportComponent } from '../../fruitreport/fruitreport.component';
// import { MealreportComponent } from '../../mealreport/mealreport.component';
import { MenuComponent } from '../../menu/menu.component';
import { LoginLogsComponent } from '../../login-logs/login-logs.component';
import { LogoutLogsComponent } from '../../logout-logs/logout-logs.component';
import { AnnouncementComponent } from '../../announcement/announcement.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,   
    ChartsModule,
    NgbModule,
    ToastrModule.forRoot()
  ],
  declarations: [
    // DashboardComponent, 
    MenuComponent,
    LoginLogsComponent,
    LogoutLogsComponent,
    AnnouncementComponent
    // FreemealComponent,
    // EmployeeComponent,
    // AccountsComponent,
    // FruitreportComponent,
    // MealreportComponent, 
  ]
})

export class AdminLayoutModule {}
