import { Routes } from '@angular/router';

// import { DashboardComponent } from '../../dashboard/dashboard.component';
import { MenuComponent } from '../../menu/menu.component';
import { LoginLogsComponent } from '../../login-logs/login-logs.component';
import { LogoutLogsComponent } from '../../logout-logs/logout-logs.component';
import { AnnouncementComponent } from '../../announcement/announcement.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'menu',component: MenuComponent },
    { path: 'loginLogs',component: LoginLogsComponent },
    { path: 'logoutLogs',component: LogoutLogsComponent },
    { path: 'announcement',component: AnnouncementComponent },


    
];
