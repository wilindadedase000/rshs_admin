import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { DataService } from '../services/data.service';
import { Credentials } from '../data-schema';
import {debounceTime} from 'rxjs/operators';
import { Observable,Subject } from 'rxjs';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  apl = new Credentials();
  tk: Object;
  vData: Object;
  private _success = new Subject<string>();
  successMessage: string;
  typeclr:string;
  constructor(private router: Router,private user: UserService, private ds: DataService) { }

  ngOnInit() {

    this.tk = JSON.parse(localStorage.getItem('tmcore'));
    if(this.user.getUserLoggedIn()){
      this.router.navigate(['menu']);
    }
    this._success.subscribe((message) => this.successMessage = message);
    this._success.pipe(
      debounceTime(4000)
    ).subscribe(() => this.successMessage = null);
  }

  signIn(e){
    // console.log(this.username.value, this.password.value);
    e.preventDefault();
    this.apl.username = e.target[0].value;
    this.apl.password = e.target[1].value;

    if(this.apl.username=="admin")
    {
      this.user.username = "Sample";
      this.user.setUserLoggedIn();
      this.router.navigate(['menu']);  
    }
    // this.ds.pullData(this.apl, "applogin").subscribe((res)=>{
    //   if(res[0].status[0].remarks=="OK"){
    //     this.vData = res[0].result;
    //     if(this.apl.username==this.vData[0])
    //     {       
    //       this.user.username = this.vData[1].emp_fname + " " + this.vData[1].emp_lname;
    //       this.user.setUserLoggedIn();

    //       localStorage.setItem('tmcore', '[{"token":"ZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUlzSW1Gd2NDSTZJa05UUlZJaUxDSmtaWFlpT2lKVVpXTm9UV0YwWlhOUVNDSjkuZXlKMVkyOWtaU0k2SW5SdE1EQXlJaXdpZFdWdFlXbHNJam9pYldWc2JtVnlZbUZzWTJWQVoyMWhhV3d1WTI5dElpd2lhV0o1SWpvaWRHVmphRzFoZEdWeklpd2lhV1VpT2lKcGJtWnZRSFJsWTJodFlYUmxjM0JvTG1OdmJTSXNJbWxrWVhSbElqcDdJbVJoZEdVaU9pSXlNREU0TFRBNExUSTVJREV6T2pBM09qTXdMamsyTWpJMk1pSXNJblJwYldWNmIyNWxYM1I1Y0dVaU9qTXNJblJwYldWNmIyNWxJam9pUlhWeWIzQmxYQzlDWlhKc2FXNGlmWDAubm5UQ3BmdVJLamNkTTlKeTZYWlhNaFhpT1VDZXEzODVYSlRjblR4Z3libw==", "vURL":"www.techmatesph.com"}]');       
    //       this.router.navigate(['menu']);       
    //     }
    //    else
    //    {
    //      this.typeclr="danger";
    //      this._success.next("Incorrect Username/Password");
    //    }
    //   }
    //   else
    //   {
    //     this.typeclr="danger";
    //     this._success.next(res[0].status[0].message);
    //   }
    // },
    // err=>{
    // this.typeclr="danger";
    // this._success.next(err.message);
    // });    
  }

}
