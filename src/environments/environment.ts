// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  config: {
    apiKey: "AIzaSyCNyPXKmPuG_UiFv_oj15iIplMVsFrSjRE",
    authDomain: "regional-science-high-school.firebaseapp.com",
    databaseURL: "https://regional-science-high-school.firebaseio.com",
    projectId: "regional-science-high-school",
    storageBucket: "regional-science-high-school.appspot.com",
    messagingSenderId: "873593454881"
  },
  production: false
};
